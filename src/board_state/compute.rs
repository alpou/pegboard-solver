use super::{BoardStateId, BoardStateVec};
use super::convert::vec_to_id;
use crate::graph::Graph;

impl BoardStateVec
{
	/// Returns the ids of all the states that are reachable in one move, given
	/// the state of the board.
	///
	/// There are six possible moves up to translation, or three up to
	/// translation and reflexion, or one up to translation and rotation (which
	/// implies reflexion).
	///
	/// In other words, all moves are the same as `110 -> 001` for three
	/// consecutive holes in a straight line, in some direction (where `1`
	/// represents a hole with a peg and `0` represents an empty hole).
	///
	/// This method considers six different patterns with three Booleans and
	/// "sweeps" or "translates" them across the board using loops.
	///
	/// # Example
	///
	/// ```
	/// # use pegboard_solver::board_state::BoardStateVec;
	/// // parent represents the following board (1 is filled, 0 is empty):
	/// //   1
	/// //  1 1
	/// // 0 0 0
	/// let parent = BoardStateVec::from(7, 3);
	///
	/// // The above has two children: id 8 and id 32, respectively representing
	/// // the following boards:
	/// //   0         0
	/// //  0 1  and  1 0
	/// // 1 0 0     0 0 1
	/// let children = vec![12, 34];
	///
	/// assert_eq!(parent.find_children_ids(), children);
	/// ```
	pub fn find_children_ids(&self) -> Vec<BoardStateId>
	{
		// Trivial case
		if self.nb_rows < 3
		{
			return Vec::new();
		}
		
		// The rest is for the non-trivial cases
		let state = &self.value; // Just a shortcut
		let mut tmp = self.value.clone();
		let mut output: Vec<BoardStateId> = Vec::new();
		
		// Note: In this outer loop, the index stops 2 before the end because
		// the pattern is 3 holes long.
		for i in 0..(self.nb_rows - 2)
		{
			for j in 0..(i + 1)
			{
				// Check for diagonal upper right
				// to lower left patterns (either 110 or 011)
				if state[i + 1][j] && state[i][j] != state[i + 2][j]
				{
					// Apply effect of move on tmp, which
					// inverts the three bits
					tmp[i    ][j] = !tmp[i    ][j];
					tmp[i + 1][j] = !tmp[i + 1][j];
					tmp[i + 2][j] = !tmp[i + 2][j];
					
					// Push corresponding id in the output
					output.push(vec_to_id(&tmp));
					
					// Revert move on tmp
					tmp[i    ][j] = !tmp[i    ][j];
					tmp[i + 1][j] = !tmp[i + 1][j];
					tmp[i + 2][j] = !tmp[i + 2][j];
				}
				
				// Check for diagonal upper left
				// to lower right patterns (either 110 or 011)
				if state[i + 1][j + 1] && state[i][j] != state[i + 2][j + 2]
				{
					// Apply effect of move on tmp, which
					// inverts the three bits
					tmp[i    ][j    ] = !tmp[i    ][j    ];
					tmp[i + 1][j + 1] = !tmp[i + 1][j + 1];
					tmp[i + 2][j + 2] = !tmp[i + 2][j + 2];
					
					// Push corresponding id in the output
					output.push(vec_to_id(&tmp));
					
					// Revert move on tmp
					tmp[i    ][j    ] = !tmp[i    ][j    ];
					tmp[i + 1][j + 1] = !tmp[i + 1][j + 1];
					tmp[i + 2][j + 2] = !tmp[i + 2][j + 2];
				}
			}
		}
		
		// Note: Similarly as in the previous outer loop, the index starts 2
		// after the beginning because the pattern is 3 holes long.
		for i in 2..self.nb_rows
		{
			for j in 0..(i - 2 + 1)
			{
				// Check for horizontal patterns
				// (either 110 or 011)
				if state[i][j + 1] && state[i][j] != state[i][j + 2]
				{
					// Apply effect of move on tmp, which
					// inverts the three bits
					tmp[i][j    ] = !tmp[i][j    ];
					tmp[i][j + 1] = !tmp[i][j + 1];
					tmp[i][j + 2] = !tmp[i][j + 2];
					
					// Push corresponding id in the output
					output.push(vec_to_id(&tmp));
					
					// Revert move on tmp
					tmp[i][j    ] = !tmp[i][j    ];
					tmp[i][j + 1] = !tmp[i][j + 1];
					tmp[i][j + 2] = !tmp[i][j + 2];
				}
			}
		}
		
		output
	}
}



/// Lists all the ids with `nb_holes` bits on total that have exactly `nb_pegs`
/// bits set to 1 and thus `nb_holes` - `nb_pegs` bits set to 0.
pub fn list_ids_with_nb_pegs(nb_pegs: usize, nb_holes: usize) -> Vec<BoardStateId>
{
	let mut output = Vec::new();
	
	/* Recursive helper
	 * - output: Vector to be filled (this is only used because functions
	 *   cannot capture their environment and closures cannot be recursive).
	 * - canvas: Number being worked on.
	 * - remaining_bits: Number of bits that need to be set to 1. This is
	 *   decreased by one with each recursion.
	 * - left_index: Index limiting after which bit new 1s can be added
	 * - nb_holes: Number of holes on the pegboard */
	fn helper(
		output: &mut Vec<BoardStateId>,
		canvas: BoardStateId,
		remaining_bits: usize,
		left_index: usize,
		nb_holes: usize
	) {
		// Base case
		if remaining_bits == 0
		{
			output.push(canvas);
		}
		
		// Recursive case
		else
		{
			/* The last bits are skipped because we want to have
			 * enough free holes to insert all the remaining bits */
			for i in left_index..=(nb_holes - remaining_bits)
			{
				helper(
					output,
					canvas | (1 << i),
					remaining_bits - 1,
					i + 1,
					nb_holes
				);
			}
		}
	}
	
	helper(&mut output, 0, nb_pegs, 0, nb_holes);
	output
}



/// Returns all the paths in `states` that lead to `child`, stored in a vec of
/// vecs of state ids.
///
/// The innermost vecs are the paths, starting from the first parentless node
/// found (going backward from the child) and ending at the input `child`.
/// The `states` input graph is assumed to contain no cycle. The presence of
/// cycles would lead to infinite recursion.
pub fn find_paths_to(states: &Graph, child: BoardStateId) -> Vec<Vec<BoardStateId>>
{
	let mut output = Vec::new();
	
	for parent in &states[child].parents
	{
		output.extend(find_paths_to(states, *parent));
	}
	
	if output.len() > 0
	{
		for path in &mut output
		{
			path.push(child);
		}
	}
	else
	{
		output.push(vec![child]);
	}
	
	output
}



#[cfg(test)]
mod tests
{
	use super::*;
	
	
	
	#[test]
	fn test_find_children_ids()
	{
		// Trivial tests
		let result = BoardStateVec::from(0, 0).find_children_ids();
		assert_eq!(result, Vec::new());
		let result = BoardStateVec::from(0, 1).find_children_ids();
		assert_eq!(result, Vec::new());
		let result = BoardStateVec::from(1, 1).find_children_ids();
		assert_eq!(result, Vec::new());
		
		/* The state
		 *     0
		 *    1 1
		 *   1 1 1
		 *  1 1 1 1
		 * 1 1 1 1 1
		 * ID: 32766
		 * has as its two children
		 *     1             1
		 *    0 1           1 0
		 *   0 1 1   and   1 1 0
		 *  1 1 1 1       1 1 1 1
		 * 1 1 1 1 1     1 1 1 1 1
		 * ID: 32757     ID: 32731
		 */
		let mut result = BoardStateVec::from(32766, 5).find_children_ids();
		result.sort();
		assert_eq!(result, vec![32731, 32757]);
		
		/* The state
		 *     0
		 *    1 1
		 *   0 0 1
		 *  1 1 1 1
		 * 1 0 1 0 1
		 * ID: 22502
		 * has as its three children
		 *     0          0               1
		 *    1 1        1 1             1 0
		 *   1 0 1  ,   1 0 1   , and   0 0 0
		 *  0 1 1 1    1 0 1 1         1 1 1 1
		 * 0 0 1 0 1  1 0 0 0 1       1 0 1 0 1
		 * ID: 21422  ID: 18286       ID: 22467
		 */
		let mut result = BoardStateVec::from(22502, 5).find_children_ids();
		result.sort();
		assert_eq!(result, vec![18286, 21422, 22467]);
		
		/* The state
		 *     0
		 *    0 0
		 *   0 1 0
		 *  0 1 1 0
		 * 0 0 0 0 0
		 *  ID: 400
		 * has as its six children
		 *     0            0            0
		 *    1 0          0 0          0 0
		 *   0 0 0  ,     0 1 0  ,     0 0 0  ,
		 *  0 1 0 0      0 0 0 1      0 0 1 0
		 * 0 0 0 0 0    0 0 0 0 0    0 1 0 0 0
		 *  ID: 130      ID: 528     ID: 2304
		 *     0            0            0
		 *    0 0          0 0          0 1
		 *   0 0 0  ,     0 1 0  ,     0 0 0
		 *  0 1 0 0      1 0 0 0      0 0 1 0
		 * 0 0 0 1 0    0 0 0 0 0    0 0 0 0 0
		 * ID: 8320      ID: 80       ID: 260
		 */
		let mut result = BoardStateVec::from(400, 5).find_children_ids();
		result.sort();
		assert_eq!(result, vec![80, 130, 260, 528, 2304, 8320]);
		
		/* The following states have no children:
		 *     0          1          1          0
		 *    0 0        0 0        1 1        1 1
		 *   0 0 0  ,   0 0 0  ,   1 1 1  ,   0 1 0
		 *  0 0 0 0    0 0 0 0    1 1 1 1    1 1 1 1
		 * 0 0 0 0 0  1 0 0 0 1  1 1 1 1 1  0 1 0 1 0
		 *   ID: 0    ID: 17409  ID: 32767  ID: 11222
		 */
		let result = BoardStateVec::from(0, 5).find_children_ids();
		assert_eq!(result, vec![]);
		let result = BoardStateVec::from(17409, 5).find_children_ids();
		assert_eq!(result, vec![]);
		let result = BoardStateVec::from(32767, 5).find_children_ids();
		assert_eq!(result, vec![]);
		let result = BoardStateVec::from(11222, 5).find_children_ids();
		assert_eq!(result, vec![]);
	}
	
	
	
	#[test]
	fn test_list_ids_with_nb_pegs()
	{
		// The numbers with all bits set to 0
		assert_eq!(list_ids_with_nb_pegs(0, 1), vec![0]);
		assert_eq!(list_ids_with_nb_pegs(0, 15), vec![0]);
		assert_eq!(list_ids_with_nb_pegs(0, 150), vec![0]);
		
		// All numbers with exactly 1 bit out of 6 set to 1
		let mut result = list_ids_with_nb_pegs(1, 6);
		result.sort();
		assert_eq!(result, vec![1, 2, 4, 8, 16, 32]);
		
		// Same as above, but on a bigger board
		let mut result = list_ids_with_nb_pegs(1, 15);
		result.sort();
		assert_eq!(result, vec![
			1, 2, 4, 8, 16, 32, 64, 128, 256,
			 512, 1024, 2048, 4096, 8192, 16384
		]);
		
		// All numbers with exactly 2 bits out of 3 set to 1
		let mut result = list_ids_with_nb_pegs(2, 3);
		result.sort();
		assert_eq!(result, vec![3, 5, 6]);
		
		// Same as above, but on a bigger board
		let mut result = list_ids_with_nb_pegs(2, 15);
		result.sort();
		assert_eq!(result, vec![
			3, 5, 6, 9, 10, 12, 17, 18, 20, 24, 33, 34, 36, 40, 48,
			65, 66, 68, 72, 80, 96, 129, 130, 132, 136, 144, 160,
			192, 257, 258, 260, 264, 272, 288, 320, 384, 513, 514,
			516, 520, 528, 544, 576, 640, 768, 1025, 1026, 1028,
			1032, 1040, 1056, 1088, 1152, 1280, 1536, 2049, 2050,
			2052, 2056, 2064, 2080, 2112, 2176, 2304, 2560, 3072,
			4097, 4098, 4100, 4104, 4112, 4128, 4160, 4224, 4352,
			4608, 5120, 6144, 8193, 8194, 8196, 8200, 8208, 8224,
			8256, 8320, 8448, 8704, 9216, 10240, 12288, 16385,
			16386, 16388, 16392, 16400, 16416, 16448, 16512, 16640,
			16896, 17408, 18432, 20480, 24576
		]);
		
		// All numbers with exactly 14 bits set to 1
		// (and thus only one 0)
		let mut result = list_ids_with_nb_pegs(14, 15);
		result.sort();
		assert_eq!(result, vec![
			16383, 24575, 28671, 30719, 31743, 32255, 32511, 32639,
			32703, 32735, 32751, 32759, 32763, 32765, 32766
		]);
		
		// The number with all bits set to 1
		assert_eq!(list_ids_with_nb_pegs(15, 15), vec![32767]);
	}
	
	
	
	#[test]
	fn test_find_paths_to()
	{
		use crate::graph::{GraphNode, TriOpt};
		
		// Build dummy states graph
		// The graph looks like the following drawing, where numbers are
		// BoardStateId's and smaller numbers are parents of bigger
		// numbers:
		//   1   4
		//  / \ / \
		// 0   3   6
		//  \ / \ /
		//   2   5
		let mut states = Graph::new(4);
		states[0] = GraphNode { // Id 0
			state: TriOpt::None,
			parents: vec![],
			children: vec![1, 2]
		};
		states[1] = GraphNode { // Id 1
			state: TriOpt::None,
			parents: vec![0],
			children: vec![3]
		};
		states[2] = GraphNode { // Id 2
			state: TriOpt::None,
			parents: vec![0],
			children: vec![3]
		};
		states[3] = GraphNode { // Id 3
			state: TriOpt::None,
			parents: vec![1, 2],
			children: vec![4, 5]
		};
		states[4] = GraphNode { // Id 4
			state: TriOpt::None,
			parents: vec![3],
			children: vec![6]
		};
		states[5] = GraphNode { // Id 5
			state: TriOpt::None,
			parents: vec![3],
			children: vec![6]
		};
		states[6] = GraphNode { // Id 6
			state: TriOpt::None,
			parents: vec![4, 5],
			children: vec![]
		};
		
		// Test on 0
		let result = find_paths_to(&states, 0);
		assert_eq!(result, vec![vec![0]]);
		
		// Test on 1
		let result = find_paths_to(&states, 1);
		assert_eq!(result, vec![vec![0, 1]]);
		
		// Test on 3
		let result = find_paths_to(&states, 3);
		assert_eq!(result, vec![
			vec![0, 1, 3],
			vec![0, 2, 3]
		]);
		
		// Test on 4
		let result = find_paths_to(&states, 4);
		assert_eq!(result, vec![
			vec![0, 1, 3, 4],
			vec![0, 2, 3, 4]
		]);
		
		// Test on 6
		let result = find_paths_to(&states, 6);
		assert_eq!(result, vec![
			vec![0, 1, 3, 4, 6],
			vec![0, 2, 3, 4, 6],
			vec![0, 1, 3, 5, 6],
			vec![0, 2, 3, 5, 6]
		]);
	}
}



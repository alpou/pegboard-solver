use super::{BoardStateId, BoardStateVec};
use std::convert::Into;



/// Conversion from `BoardStateId` to `BoardStateVec`.
impl BoardStateVec {
	// Note: This used to implement std::convert::From, but a second input was
	// necessary for nb_rows, so it became a regular public method.
	pub fn from(item: BoardStateId, nb_rows: usize) -> Self {
		BoardStateVec {
			nb_rows,
			value: id_to_vec(item, nb_rows)
		}
	}
}

/// Conversion from `BoardStateVec` to `BoardStateId`.
impl Into<BoardStateId> for BoardStateVec {
	fn into(self) -> BoardStateId {
		vec_to_id(&self.value)
	}
}



/// Helper function for `BoardStateVec::from`.
///
/// Produces a new `BoardStateVec` associated to a `BoardStateId`.
/// The returned `BoardStateVec` has `nb_rows` rows.
///
/// # Example
///
/// ```
/// # use pegboard_solver::board_state::convert::id_to_vec;
/// let board_id = 40;
/// let board_vec = vec![
/// 	vec![false],
/// 	vec![false, false],
/// 	vec![true,  false, true]
/// ];
///
/// assert_eq!(id_to_vec(board_id, board_vec.len()), board_vec);
/// ```
pub fn id_to_vec(mut id: BoardStateId, nb_rows: usize) -> Vec<Vec<bool>>
{
	let mut boardstate = Vec::new();
	
	for i in 0..nb_rows
	{
		boardstate.push(Vec::new());
		
		for _ in 0..(i + 1) // Row length = i + 1
		{
			boardstate[i].push(id % 2 == 1);
			id >>= 1;
		}
	}
	
	boardstate
}



/// Helper function for `BoardStateVec::Into<BoardStateId>`.
///
/// Computes the `BoardStateId` of a `BoardStateVec`.
///
/// # Example
///
/// ```
/// # use pegboard_solver::board_state::convert::vec_to_id;
/// let board_id = 40;
/// let board_vec = vec![
/// 	vec![false],
/// 	vec![false, false],
/// 	vec![true,  false, true]
/// ];
///
/// assert_eq!(vec_to_id(&board_vec), board_id);
/// ```
pub fn vec_to_id(state: &Vec<Vec<bool>>) -> BoardStateId
{
	let mut output = 0;
	let mut mask = 1;
	
	for row in state
	{
		for hole in row
		{
			if *hole // has a peg in it
			{
				output |= mask;
			}
			mask <<= 1;
		}
	}
	
	output
}



#[cfg(test)]
mod tests
{
	use super::*;
	
	#[test]
	fn test_id_to_vec()
	{
		assert_eq!(id_to_vec(0, 0), Vec::<Vec<bool>>::new());
		
		assert_eq!(id_to_vec(0, 1), vec![
			vec![false]
		]);
		
		assert_eq!(id_to_vec(0, 2), vec![
			vec![false],
			vec![false, false]
		]);
		
		assert_eq!(id_to_vec(0, 3), vec![
			vec![false],
			vec![false, false],
			vec![false, false, false]
		]);
		
		assert_eq!(id_to_vec(0, 4), vec![
			vec![false],
			vec![false, false],
			vec![false, false, false],
			vec![false, false, false, false]
		]);
		
		assert_eq!(id_to_vec(0, 5), vec![
			vec![false],
			vec![false, false],
			vec![false, false, false],
			vec![false, false, false, false],
			vec![false, false, false, false, false]
		]);
		
		assert_eq!(id_to_vec(7, 5), vec![
			vec![true],
			vec![true, true],
			vec![false, false, false],
			vec![false, false, false, false],
			vec![false, false, false, false, false]
		]);
		
		assert_eq!(id_to_vec(698, 5), vec![
			vec![false],
			vec![true,  false],
			vec![true,  true, true],
			vec![false, true, false, true],
			vec![false, false, false, false, false]
		]);
		
		assert_eq!(id_to_vec(26408, 5), vec![
			vec![false],
			vec![false, false],
			vec![true,  false, true],
			vec![false, false, true,  true],
			vec![true,  false, false, true, true]
		]);
		
		assert_eq!(id_to_vec(32767, 5), vec![
			vec![true],
			vec![true, true],
			vec![true, true, true],
			vec![true, true, true, true],
			vec![true, true, true, true, true]
		]);
		
		assert_eq!(id_to_vec(32767, 7), vec![
			vec![true],
			vec![true,  true],
			vec![true,  true,  true],
			vec![true,  true,  true,  true],
			vec![true,  true,  true,  true,  true],
			vec![false, false, false, false, false, false],
			vec![false, false, false, false, false, false, false]
		]);
	}
	
	
	
	// Tests also with non-standard sizes
	#[test]
	fn test_vec_to_id()
	{
		// Simple exhaustive tests
		assert_eq!(0, vec_to_id(&vec![vec![false], vec![false, false]]));
		assert_eq!(1, vec_to_id(&vec![vec![true],  vec![false, false]]));
		assert_eq!(2, vec_to_id(&vec![vec![false], vec![true,  false]]));
		assert_eq!(3, vec_to_id(&vec![vec![true],  vec![true,  false]]));
		assert_eq!(4, vec_to_id(&vec![vec![false], vec![false, true]]));
		assert_eq!(5, vec_to_id(&vec![vec![true],  vec![false, true]]));
		assert_eq!(6, vec_to_id(&vec![vec![false], vec![true,  true]]));
		assert_eq!(7, vec_to_id(&vec![vec![true],  vec![true,  true]]));
		
		// More complicated tests
		let board = vec![
			vec![false],
			vec![true,  false],
			vec![true,  true, true],
			vec![false, true, false, true]
		];
		assert_eq!(698, vec_to_id(&board));
		
		let board = vec![
			vec![false],
			vec![false, false],
			vec![true,  false, true],
			vec![false, false, true,  true],
			vec![true,  false, false, true, true]
		];
		assert_eq!(26408, vec_to_id(&board));
		
		let board = vec![
			vec![false],
			vec![false, false],
			vec![false, false, true],
			vec![false, false, true,  true],
			vec![true,  true,  false, false, true],
			vec![true,  false, true,  true,  true, false],
			vec![false, false, false, false, true, true, false]
		];
		assert_eq!(101633824, vec_to_id(&board));
	}
}



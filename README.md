# Pegboard Solver

A program to find solutions to a classic puzzle with a board in which pegs are
inserted.

## Description of the problem

The puzzle consists of a board and several pegs.

The board has round holes that are equally spaces and all identical. These are
placed in rows and form the shape of an equilateral triangular. The first row
has one hole, the second one has two holes, and so on, down to the last row,
which has as many holes as there are rows. The number of rows can vary.

The pegs are identical and are designed to fit in the holes and stay upright
when inserted. There is one peg fewer than there are holes.

The goal of the puzzle is to start with one empty hole and eliminate the pegs
until there are as few as possible remaining on the board without any possible
elimination.

The elimination of pegs works as follows.
Consider a row of three consecutive holes in any direction. If there are pegs
in the first and the second holes, and the third one is empty, then the peg in
the first hole can jump over the one in the second hole and land in the third
one, eliminating the other peg.
This manipulation has the effect of changing two filled holes and an empty hole
(in this order) into two empty holes and a filled hole (in this order).

If we represent visually a filled hole with `X` and an empty hole with `O`, then
this transition changes three consecutive holes from `X X O` to `O O X`.

## How to use the program

To use this program, make sure to have Git, Rust, and Cargo installed.
For example, on Arch-based distributions of Linux, simply install the `git` and
`rust` packages:

    # pacman -S git rust

Clone this repository on your machine and step in it:

    $ git clone https://gitlab.com/alpou/pegboard-solver.git
    $ cd pegboard-solver

Then, run `cargo run` to execute the program directly, or

    $ cargo build --release

to compile the program, which can then be executed as
`./target/release/pegboard-solver`.

## How this program approaches the problem

For a given board with <i>n</i> holes, the set of all possible peg
configurations is isomorphic to the set of all unsigned integers with <i>n</i>
bits, that is {0, 1, ..., 2<sup><i>n</i></sup> - 1}. We can therefore allocate
a data structure with 2<sup><i>n</i></sup> items and have enough space for all
possible peg configurations. In this work, these configurations are called
<i>board states</i> and the associated integers are called <i>ids</i>.

The way this program maps the states to the ids is the following.
Filled holes are mapped to 1s and empty holes are mapped to 0s.
Then, to determine which hole is associated with which bit,
the hole in the first row of the board is mapped to the 0<sup>th</sup> bit (the
least significant bit) of the id;
the left hole in the second row is mapped to the 1<sup>st</sup> bit;
the right hole in the second row is mapped to the 2<sup>nd</sup> bit;
and so on down to the rightmost hole in the bottom row, which is mapped to the
most significant bit of the id.

In other words, the order of the holes going from the one associated with the
least significant bit to the one associated with the most significant bit is the
following:

        0
       1 2
      3 4 5
       ...
     ... n-2 n-1

The functions in this program that convert an id to the associated board state
and vice versa are respectively `id_to_vec` and `vec_to_id`, defined in
`board_state::convert`.

As mentioned above, a data structure is allocated big enough to contain all
possible board states. In the source code, it is called `states` and is defined
in the main function. It is a vector of `GraphNode` structures. The index of
each of them in the vector is equal to the id of the board state it represents.
Each structure holds a vector of its parents (ids of the states from which it is
possible to reach the state represented by the structure) and a vector of its
children (ids of the states that are reachable from the state).

In order to populate the states data structure, initial states are defined as
all the states with all but one holes filled. The ids of these initial states
are then inserted in a queue where the ids of the states to process are stored.
When an id is popped from the queue, all the possible moves are performed on the
associated state to compute its children. The ids of these children are then
inserted in the queue if they were not already inserted or processed.

After the transitions between the states are computed, the childless states with
the requested number of pegs are enumerated and the paths from the initial
states to the childless states are printed.

## TODO

- Parameters should be read from CLI first, and then asked for interactively if
  not provided (as is the case now).

## Licensing

The content of this repository is provided under the GPLv3 license. See
the included LICENSE file for more information.

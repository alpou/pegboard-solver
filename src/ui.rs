use std::io::{stdin, stdout, Write};
use crate::graph::{Graph, TriOpt};



/// Prints some stats about the states visited during the search.
///
/// Returns `Ok(())` if everything went well, or an `Err` with the index of the
/// first node that is still unprocessed if one is found. In the latter case,
/// nothing is printed to `stdout`.
pub fn print_stats(states: &Graph) -> Result<(), usize>
{
	let mut nb_accessible = 0;
	let mut nb_inaccessible = 0;
	for (i, node) in states.iter().enumerate()
	{
		match node.state
		{
			TriOpt::None => nb_inaccessible += 1,
			TriOpt::Some(_) => nb_accessible += 1,
			TriOpt::InQueue => return Err(i),
		}
	}
	let perc_accessible   = nb_accessible   as f32/states.nb_states as f32 * 100.0;
	let perc_inaccessible = nb_inaccessible as f32/states.nb_states as f32 * 100.0;
	println!(
		"{}/{} ({:.2}%) accessible states, {}/{} ({:.2}%) inaccessible states",
		nb_accessible,   states.nb_states, perc_accessible,
		nb_inaccessible, states.nb_states, perc_inaccessible,
	);
	Ok(())
}



/// Interactively asks the user for a number of rows on the board (verified to
/// be at least 3 and at most `max_nb_rows`) or the letter `'q'` to quit.
///
/// Keeps asking until the answer is valid.
///
/// This function reads from `stdin` and prints to `stdout`.
///
/// Returned value is either `Some` with a valid choice or `None` if the user
/// wants to quit.
///
/// # Panics
///
/// A panic can occur if `stdin().read_line` returns an error.
pub fn ask_nb_rows(max_nb_rows: usize) -> Option<usize>
{
	println!("Please specify the number of rows on the board, or q to quit");
	let mut input = String::new();
	loop
	{
		print!("> ");
		let _ = stdout().flush();
		
		input.clear(); // Need to clear because read_line appends
		stdin().read_line(&mut input)
			.expect("Error when reading input");
		
		let input = input.trim(); // Remove whitespaces
		
		if input == "q" {
			return None;
		}
		
		let input_opt: Result<usize, _> = input.parse();
		match input_opt
		{
			Err(_) => println!("Invalid input"),
			Ok(choice) => if choice < 3 || max_nb_rows < choice {
				println!(
					"Invalid number, needs to be at least 3 and at most {}",
					max_nb_rows
				)
			} else {
				return Some(choice);
			}
		}
	}
}



/// Interactively asks the user for a number of pegs remaining (verified to be
/// between 1 inclusively and `nb_holes` exclusively) or the letter `'q'` to
/// quit.
///
/// Keeps asking until the answer is valid.
///
/// This function reads from `stdin` and prints to `stdout`.
///
/// Returned value is either `Some` with a valid choice or `None` if the user
/// wants to quit.
///
/// # Panics
///
/// A panic can occur if `stdin().read_line` returns an error.
pub fn ask_nb_remaining_pegs(nb_holes: usize) -> Option<usize>
{
	println!("Please specify the number of pegs in the childless states for which you");
	println!("would like to see the transitions, or q to quit");
	let mut input = String::new();
	loop
	{
		print!("> ");
		let _ = stdout().flush();
		
		input.clear(); // Need to clear because read_line appends
		stdin().read_line(&mut input)
			.expect("Error when reading input");
		
		let input = input.trim(); // Remove whitespaces
		
		if input == "q" {
			return None;
		}
		
		let input_opt: Result<usize, _> = input.parse();
		match input_opt
		{
			Err(_) => println!("Invalid input"),
			Ok(choice) => if choice <= 0 || nb_holes <= choice {
				println!(
					"Invalid number, needs to be between 1 and {} inclusively",
					nb_holes - 1
				)
			} else {
				return Some(choice);
			}
		}
	}
}



use std::ops::{Index, IndexMut};
use std::slice::Iter;
use crate::board_state::{BoardStateId, BoardStateVec};



/// Returns the maximum possible value of `nb_rows` for the system's `usize`.
///
/// The exact value would be the floor of
/// `(sqrt(1 + 8 log2(usize::MAX)) - 1)/2`.
/// However, replacing `log2(usize::MAX)` with `usize::BITS` yields a very
/// good approximation. So good, in fact, that even before computing the
/// floor, the two values are the same up to at least 15 decimals.
///
/// # Examples
///
/// ```
/// # use pegboard_solver::graph::get_max_nb_rows;
/// if usize::BITS == 64 {
/// 	assert_eq!(get_max_nb_rows(), 10);
/// } else if usize::BITS == 32 {
/// 	assert_eq!(get_max_nb_rows(), 7);
/// }
/// ```
pub fn get_max_nb_rows() -> usize
{
	let tmp = (1 + 8 * usize::BITS) as f64;
	(tmp.sqrt().floor() as usize - 1)/2
}



/// Keeps track of the tasks in the transitions graph.
#[derive(Clone)]
pub enum TriOpt
{
	/// Nothing done so far
	None,
	/// Enqueued in `task_queue`
	InQueue,
	/// Processed, with the associated board vec
	Some(BoardStateVec)
}

/// Node in a `Graph`.
#[derive(Clone)]
pub struct GraphNode
{
	pub state: TriOpt,
	pub parents: Vec::<BoardStateId>,
	pub children: Vec::<BoardStateId>
}

impl GraphNode
{
	pub fn new() -> Self
	{
		GraphNode {
			state: TriOpt::None,
			parents: Vec::<BoardStateId>::new(),
			children: Vec::<BoardStateId>::new()
		}
	}
}

/// Graph used as the main data structure of the program.
pub struct Graph
{
	pub states: Vec<GraphNode>,
	pub nb_rows: usize, // Number of rows on the board
	pub nb_holes: usize, // Number of holes on the board
	pub nb_states: usize, // Number of possible states
}

impl Graph
{
	pub fn new(nb_rows: usize) -> Self
	{
		// Note: \sum_{i = 1}^n i = \frac{n^2 + n}{2}
		let nb_holes = (nb_rows * nb_rows + nb_rows)/2;
		// Note: The line below is equivalent to 2**nb_holes,
		// where ** is exponentiation
		let nb_states = 1 << nb_holes;
		
		Graph {
			states: vec![GraphNode::new(); nb_states],
			nb_rows,
			nb_holes,
			nb_states,
		}
	}
	
	/// Returns an iterator on the internal `states` member.
	pub fn iter(&self) -> Iter<'_, GraphNode>
	{
		self.states.iter()
	}
}

/// Immutably indexing a `Graph` simply indexes its `states` member.
impl Index<BoardStateId> for Graph
{
	type Output = GraphNode;
	
	fn index(&self, index: BoardStateId) -> &Self::Output {
		&self.states[index]
	}
}

/// Mutably indexing a `Graph` simply indexes its `states` member.
impl IndexMut<BoardStateId> for Graph
{
	fn index_mut(&mut self, index: BoardStateId) -> &mut Self::Output {
		&mut self.states[index]
	}
}



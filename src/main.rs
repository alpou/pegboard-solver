use std::collections::VecDeque;
use pegboard_solver::board_state::{BoardStateId, BoardStateVec};
use pegboard_solver::board_state::compute::{
	list_ids_with_nb_pegs, find_paths_to
};
use pegboard_solver::graph::{get_max_nb_rows, Graph, TriOpt};
use pegboard_solver::ui;

fn main()
{
	// TODO Add possibility to provide this value through CLI argument
	let nb_rows = match ui::ask_nb_rows(get_max_nb_rows())
	{
		Some(n) => n,
		None => return ()
	};
	
	print!("Building states graph... ");
	
	/* This graph will be filled with the possible board states.
	 * It is implemented as a vector of structures representing the graph
	 * nodes. Each node holds one vector of indices for
	 * its parents (i.e. the nodes from which we can reach the current node)
	 * and one for its children (i.e. the nodes that are accessible from
	 * the current node).
	 * Furthermore, the `state` member of each graph node is an enum that
	 * can either be None if the node was not processed, InQueue if it was
	 * placed in task_queue, or Some(BoardStateVec) if it was processed. */
	let mut states = Graph::new(nb_rows);
	
	/* The possible initial states are those that have pegs in all holes
	 * except one, since the game comes with one peg fewer than the number
	 * of holes. Thus this vector is initialized with all the numbers that
	 * have all but one bits set to 1. */
	let mut initial_states: Vec<BoardStateId> = vec![states.nb_states - 1; states.nb_holes];
	for i in 0..initial_states.len()
	{
		initial_states[i] ^= 1 << i;
	}
	
	// Queue with the BoardStateIds of the states to process
	let mut task_queue = VecDeque::from(initial_states.clone());
	
	while task_queue.len() > 0
	{
		let curr_board_id = task_queue.pop_front()
			.expect("VecDeque::pop_front returned None with len > 0");
		let curr_board_vec = BoardStateVec::from(curr_board_id, nb_rows);
		
		// Insert children of the current state in task_queue
		for child_id in curr_board_vec.find_children_ids()
		{
			states[curr_board_id].children.push(child_id);
			states[child_id].parents.push(curr_board_id);
			if let TriOpt::None = states[child_id].state {
				task_queue.push_back(child_id);
				states[child_id].state = TriOpt::InQueue;
			}
		}
		
		states[curr_board_id].state = TriOpt::Some(curr_board_vec);
	}
	
	println!("Done.");
	if let Err(i) = ui::print_stats(&states)
	{
		panic!(
			"Node {} still has state InQueue after transitions are computed. Please fix the code",
			i
		);
	}
	
	
	
	// Ask the user which states are to be reached
	// TODO Add possibility to provide this value through CLI argument
	let nb_rem_pegs = match ui::ask_nb_remaining_pegs(states.nb_holes)
	{
		Some(n) => n,
		None => return ()
	};
	
	let end_ids = list_ids_with_nb_pegs(nb_rem_pegs, states.nb_holes);
	
	
	
	// Find the paths from the initial states to the end states
	print!("Finding paths... ");
	let mut paths = Vec::new();
	for end_id in end_ids
	{
		// Skip this state if it has children,
		// we only want paths leading to dead-end states
		if states[end_id].children.len() > 0
		{
			continue;
		}
		
		// `find_paths_to` returns all the paths from a parentless state
		// to end_id
		for path in find_paths_to(&states, end_id)
		{
			if let Some(first) = path.get(0)
			{
				// So we need to make sure the first state in
				// the returned path is one of the initial
				// states of the problem
				if initial_states.contains(first)
				{
					paths.push(path);
				}
			}
		}
	}
	println!("Done.");
	
	
	
	// Print message if no path was found
	if paths.len() == 0
	{
		println!(
			"There is no path leading to childless states with {} peg{}",
			nb_rem_pegs, if nb_rem_pegs > 1 {"s"} else {""}
		);
	}
	
	// Print the paths to the chosen end states
	for (i, path) in paths.iter().enumerate()
	{
		println!("\nPath #{}/{}", i + 1, paths.len());
		for state in path
		{
			println!("{}\n", BoardStateVec::from(*state, nb_rows));
		}
	}
}



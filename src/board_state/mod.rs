use std::fmt;

pub mod convert;
pub mod compute;

/// Type definition for the ids of the board states
pub type BoardStateId = usize;

/// Data structure representing the board states.
///
/// The number of rows is included in each `BoardStateVec` even if it's the
/// same for all instances because it makes some parts of the code simpler and
/// more legible.
#[derive(Clone, Debug, PartialEq)]
pub struct BoardStateVec {
	nb_rows: usize,
	value: Vec<Vec<bool>>
}



/// Allows `println!("{}", ...)` to display an ASCII rendering of the board
/// state
impl fmt::Display for BoardStateVec
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
	{
		let state = &self.value; // Just a shortcut
		let mut p = state.len();
		let mut output = String::new();
		
		// If state is not empty
		if let Some((last_row, other_rows)) = state.split_last()
		{
			// All rows except the last
			for row in other_rows
			{
				// Space padding on the left
				for _ in 1..p {
					output.push(' ');
				}
				p -= 1;
				
				// The elements of the row
				let mut j = 0;
				while j < row.len()
				{
					output.push(if row[j] {'1'} else {'0'});
					output.push(if j < row.len() - 1 {' '} else {'\n'});
					j += 1;
				}
			}
			
			// Last row
			// No padding on the left for the last row
			let mut j = 0;
			while j < last_row.len()
			{
				output.push(if last_row[j] {'1'} else {'0'});
				if j < last_row.len() - 1 {
					output.push(' ');
				}
				j += 1;
			}
		}
		
		write!(f, "{}", output)
	}
}


